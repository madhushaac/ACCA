<?php require 'global-header.php'; ?>

<header>	
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <div id="logo"><a href="<?php echo base_url("portal"); ?>"><img src="<?php echo base_url('public/images/logo-landing.jpg'); ?>"></a></div>
            </div>
            <div class="col-xs-12 col-md-6" id="title">
                <h1>ACCA Online Voting Portal</h1>
            </div>
            <div class="col-xs-12 col-md-3 user">
                <img src="<?php echo base_url('public/images/user.png'); ?>">

                <div id="member">
                    <h3>Hello <?php echo $username; ?>!</h3>
                    <form id="" role="form" method="post" action="<?php echo base_url('login/user_logout'); ?>">
                        <input class="btn" type="submit" value="Logout" name="logout" style="position: absolute; right: 20px; top: 2px;">
                    </form>
                </div>
            </div> 
        </div>
    </div>
</header>

<section class="container content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <h2>Manage candidates</h2>
        </div>

        <div class="col-xs-12 col-md-6">
            <div class="row">
                <table class="table table-striped candidate-records">
                    <thead>
                        <tr>
                            <th>Candidate Id</th>
                            <th>Candidate Name</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if ($candidates && is_array($candidates)) {

                            foreach ($candidates as $candidate) {
                                $userBlock = '<tr data-id="' . $candidate['user_id'] . '">
                                                    <td>' . $candidate['user_id'] . '</td>
                                                    <td>' . $candidate['first_name'] . ' ' . $candidate['last_name'] . '</td>
                                                    <td><input class="btn user-edit" type="submit" data-id="' . $candidate['user_id'] . '" value="Edit" name="edit"></td>
                                                    <td><input class="btn user-delete" type="submit" data-id="' . $candidate['user_id'] . '" value="Delete" name="delete"></td>
                                                  </tr>';

                                echo $userBlock;
                            }
                        }
                        ?>  
                    </tbody>
                </table>

            </div>  
        </div>

    </div>

</section>

<div class="modal fade" id="candidate-edit-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="vote-confirmation-title">Edit Candidate Details</h5>
                <i class="fa fa-check"></i>
            </div>
            <div class="modal-body">

                <form role="form" id="update-candidates" method="post" action="<?php echo base_url('candidates/updateCandidates'); ?>" autocomplete="off">
                    <fieldset>
                        <div class="form-group">
                            <select class="form-control" id="title" name="title">
                                <optgroup autofocus>
                                    <option value="Mr">Mr</option>
                                    <option value="Mrs">Mrs</option>
                                    <option value="Miss">Miss</option>
                                </optgroup>
                            </select>
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="first name" id="first_name" name="first_name" type="text" value="" required autocomplete="off">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="last name" id="last_name" name="last_name" type="text" value="">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="qualifications" id="qualifications" name="qualifications" type="text" value="">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Designation" id="designation" name="designation" type="text" value="">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Company" id="company" name="company" type="text" value="">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="From" id="fromDate" name="from" type="text" value="">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="to" id="toDate" name="to" type="text" value="">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="service details" id="service_details" name="service_details" type="text" value="">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Address" id="address" name="address" type="text" value="">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Nominated Presenter" id="n_presenter" name="n_presenter" type="text" value="">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Nominated seconder" id="n_seconder" name="n_seconder" type="text" value="">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" placeholder="Boigraphy" id="biography" name="biography"></textarea>
                        </div>
                        <input id="submit" type="submit" value="UPDATE" name="update" >

                    </fieldset>
                </form>

            </div>
        </div>
    </div>
</div>

<?php require 'global-footer.php'; ?>