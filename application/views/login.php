<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>ACCA | Think Ahead</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/icon" href="images/favicon.ico"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <link href="<?php echo base_url('public/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('public/css/style-signin.css'); ?>" rel="stylesheet">
	  <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,800,900" rel="stylesheet">
 
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
  <header>	
  	<div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-3">
          <div id="logo"><img src="<?php echo base_url('public/images/logo-landing.jpg'); ?>"></div>
        </div>
        <div class="col-xs-12 col-md-6" id="title">
          <h1>ACCA Online Voting Portal</h1>
        </div>
        <div class="col-xs-12 col-md-3">
          
        </div> 
      </div>
  	</div>
  </header>

  <section class="container">
      <div class="row">
        <div class="col-xs-12 col-md-4">
         
        </div>
        <div class="col-xs-12 col-md-4">
            <form method="post" id="login-form" action="<?php echo base_url('login/login_user'); ?>" autocomplete="off">
            <img src="<?php echo base_url('public/images/user.png'); ?>">
            <div class="input-container required username">
              <i class="fa fa-user icon"></i>
              <input class="input-field " type="text" id="mem-id" placeholder="Member Id" name="member_id" autocomplete="off">
            </div>

            <div class="input-container required password">
              <i class="fas fa-lock icon"></i>
              <input class="input-field" type="password" id="password" placeholder="Password" name="password" autocomplete="off">
            </div>

            <p>Couldn’t login? <a href="">let us know.</a></p>
            <?php
                if(isset($log_message)){ ?>
                <p class="log-error-message"><?=$log_message?></a></p>
            <?php    }
            ?>
            
            <input id="submit" type="submit" value="SIGN IN" name="login" >
          </form>
        </div>
        <div class="col-xs-12 col-md-4">
          
        </div>
      </div>

  </section>


			
	<!-- Start footer -->
	<footer id="contentinfo" role="contentinfo">
			<div class="container ">
          <div class="row">
            <div id="logo-bte" class="col-xs-12 col-sm-2 col-md-2">
              <img src="<?php echo base_url('public/images/logo-bte.jpg'); ?>">
            </div>
            <div id="text-copyright" class="col-xs-12 col-sm-8 col-md-8">
              <p>Online Voting Portal - 1.0.0.0  Ⓒ 2018 Solution by SyncBridge</p>
            </div>
            <div id="logo-syncbridge" class="col-xs-12 col-sm-2 col-md-2">
              <img src="<?php echo base_url('public/images/logo-syncbridge.jpg'); ?>">
            </div>
          </div>
			</div>

	</footer>
	
  <script src="<?php echo base_url('public/js/jquery-2.2.4.min.js'); ?>"></script>
  <script src="<?php echo base_url('public/js/login.js'); ?>"></script>
  <script src="<?php echo base_url('public/js/bootstrap.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('public/js/jquery.countdown.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('public/js/custom.js'); ?>"></script>

	
	
    
  </body>
</html>