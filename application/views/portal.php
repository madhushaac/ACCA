<?php require 'global-header.php'; ?>

<header>	
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <div id="logo"><a href="<?php echo base_url("portal"); ?>"><img src="<?php echo base_url('public/images/logo-landing.jpg'); ?>"></a></div>
            </div>
            <div class="col-xs-12 col-md-6" id="title">
                <h1>ACCA Online Voting Portal</h1>
            </div>
            <div class="col-xs-12 col-md-3 user">
                <img src="<?php echo base_url('public/images/user.png'); ?>">

                <div id="member">
                    <h3>Hello <?php echo $username; ?>!</h3>
                    <form id="" role="form" method="post" action="<?php echo base_url('login/user_logout'); ?>">
                        <input class="btn" type="submit" value="Logout" name="logout" style="position: absolute; right: 20px; top: 2px;">
                    </form>
                </div>
                <?php if ($user_id && $user_id == SUPER_USER_ID) { ?>
                    <a href="candidates/candidates-addpanel">Add candidates</a>
                    <a href="candidates/manage-candidates" style="display: block">Manage candidates</a>
                    <a href="vote/show-votes" style="display: block">Show results</a>
                    <!--<a href="user/addUsers" style="display: block">Add users</a>-->
                    <!--<a href="user/download-users" style="display: block">Users Report</a>-->
                <?php }
                ?>

            </div> 
        </div>
    </div>
</header>

<section class="container content">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <h2>Election Title - Ballot 2018</h2>
        </div>
        <?php
        if ($candidates && is_array($candidates)) {
            foreach ($candidates as $candidate) {
                
                $fromDate = ($candidate['from_date']) ? $candidate['from_date'] : '-';
                if($voteStatus && $voteStatus['vote_status'] == 1){
                    $votedClass = 'vote-disabel';
                }else{
                    $votedClass = '';
                }
                
                $userBlock = '<div class="col-xs-12 col-md-12 candidate-holder" data-candidate-id="' . $candidate['user_id'] . '">
          <div class="row">
            <div class="col-xs-12 col-md-2">
              <img src="' . base_url('public/images/user.png') . '">
            </div>
            <div class="col-xs-12 col-md-3 details">
              <h2 class="candidate-name">' . $candidate['first_name'] . ' ' . $candidate['last_name'] . '</h2>
              <h3>' . $candidate['designation'] . '</h3>
              <p>Member ID: ' . $candidate['user_id'] . '(Since ' . $fromDate . ')</p>
              <p id="profile" class="view-profile" data-candidate-id="' . $candidate['user_id'] . '">View Full Profile</p>
            </div>
            <div class="col-xs-12 col-md-2">

            </div>
            <div class="col-xs-12 col-md-5">
              <button data-candidate-id="' . $candidate['user_id'] . '" class="vote vote-button '.$votedClass.'">VOTE <i class="far fa-thumbs-up"></i></button>
            </div>
          </div>  
        </div>';

                echo $userBlock;
            }
        }
        ?>
        <div class="col-xs-12 col-md-12">
            <button id="confirm-vote" class="disabled">CONFIRM VOTE <i class="fa fa-check"></i></button>
        </div>

    </div>

</section>



<div class="modal fade " id="profile-modal" tabindex="-1" role="dialog" aria-labelledby="profile-modal" aria-hidden="true">

</div>

<!-- vote confirmation -->

<div class="modal fade" id="vote-confirmation-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="vote-confirmation-title">VOTE CONFIRMATION</h5>
                <i class="fa fa-check"></i>
            </div>
            <div class="modal-body">

                <h2>Election Title - Ballot 2018</h2>
                <h3>You have voted for following three nominess!</h3>
                <div class="voted-candidates">
                    <p>Mr. Jhone Doe</p>
                    <p>Mr. Jhone Doe</p>
                    <p>Mr. Jhone Doe</p>
                </div>

            </div>
            <div class="modal-footer">
                <button id="vote-cancle">CANCLE</button>
                <button id="vote-confirm">CONFIRM</button>
            </div>
        </div>
    </div>
</div>

<!-- vote success -->

<div class="modal fade" id="vote-success-popup" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="vote-confirmation-title">VOTE SUCCESS</h5>
                <i class="fa fa-check"></i>
            </div>
            <div class="modal-body">

                <h2>Your Voting Results are successfully submitted.</h2>
                <h3>Results will be notified on Day–Month-Year.</h3>
                <p>Thank You!</p>
            </div>
            <div class="modal-footer">
                <button id="vote-return">RETURN TO VOTING DASHBOARD</button>
                <form id="" role="form" method="post" action="<?php echo base_url('login/user_logout'); ?>">
                    <input id="vote-confirm" class="btn" type="submit" value="LOGOUT" name="logout">
                </form>

            </div>
        </div>
    </div>
</div>

<?php require 'global-footer.php'; ?>