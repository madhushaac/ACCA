<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>ACCA | Think Ahead</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/icon" href="images/favicon.ico"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url('public/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('public/css/style-landing.css'); ?>" rel="stylesheet">
	  <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,800,900" rel="stylesheet">
 
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

<div class="container vbottom">

    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div id="logo-landing"><img src="<?php echo base_url('public/images/logo-landing.jpg'); ?>"/>
        </div>
        <div id="text-landing" class="col-xs-12 col-md-12">
            <h1>ACCA Online Voting Portal</h1>
            <h2>Election Title - Ballot 2018</h2>
            <p>will commence on 08-06-2018</p>
        </div>

        <div class="col-xs-12 col-md-12">
            <div class="event-counter-area">
                <div id="event-counter"></div>
            </div>
        </div>  
    </div>

</div>

<?php require 'global-footer.php'; ?>
