<?php require 'global-header.php'; ?>

        <header>	
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-3">
                        <div id="logo"><a href="<?php echo base_url("portal"); ?>"><img src="<?php echo base_url('public/images/logo-landing.jpg'); ?>"></a></div>
                    </div>
                    <div class="col-xs-12 col-md-6" id="title">
                        <h1>ACCA Online Voting Portal</h1>
                    </div>
                    <div class="col-xs-12 col-md-3 user">
                        <img src="<?php echo base_url('public/images/user.png'); ?>">

                        <div id="member">
                            <h3>Hello <?php echo $username; ?>!</h3>
                            <form id="" role="form" method="post" action="<?php echo base_url('login/user_logout'); ?>">
                                <input class="btn" type="submit" value="Logout" name="logout" style="position: absolute; right: 20px; top: 2px;">
                            </form>
                        </div>
                    </div> 
                </div>
            </div>
        </header>

        <section class="container content">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <h2>Show votes</h2>
                </div>
                
                <div class="col-xs-12 col-md-6">
                    <div class="row">
                        <table class="table table-striped candidate-records">
                            <thead>
                              <tr>
                                <th>Candidate Id</th>
                                <th>Candidate Name</th>
                                <th>Vote Count</th>
                              </tr>
                            </thead>
                            <tbody>
                            <?php
                            
                            if($votes && is_array($votes)){

                                foreach ($votes as $vote){
                                    $userBlock = '<t">
                                                    <td>'. $vote['candidate_id'] .'</td>
                                                    <td>'. $vote['first_name'] .'</td>
                                                    <td>'. $vote['votes'] .'</td>
                                                   </tr>';
                                    
                                    echo $userBlock;
                                }
                                
                            }
                            
                            ?>  
                            </tbody>
                        </table>
        
                    </div>  
                </div>
                <div class="col-xs-12 col-md-6">
                    
                </div>

            </div>

        </section>


        <?php require 'global-footer.php'; ?>