<!-- Start footer -->
        <footer id="contentinfo" role="contentinfo">
            <div class="container ">
                <div class="row">
                    <div id="logo-bte" class="col-xs-12 col-sm-2 col-md-2">
                        <img src="<?php echo base_url('public/images/logo-bte.jpg'); ?>">
                    </div>
                    <div id="text-copyright" class="col-xs-12 col-sm-8 col-md-8">
                        <p>Online Voting Portal - 1.0.0.0  Ⓒ 2018 Solution by SyncBridge</p>
                    </div>
                    <div id="logo-syncbridge" class="col-xs-12 col-sm-2 col-md-2">
                        <img src="<?php echo base_url('public/images/logo-syncbridge.jpg'); ?>">
                    </div>
                </div>
            </div>

        </footer>

        <script src="<?php echo base_url('public/js/jquery-2.2.4.min.js'); ?>"></script>
        <script src="<?php echo base_url('public/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('public/js/candidates.js'); ?>"></script>
        <script src="<?php echo base_url('public/js/votes.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('public/js/jquery.countdown.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('public/js/custom.js'); ?>"></script>

    </body>
</html>