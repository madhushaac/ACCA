<?php require 'global-header.php'; ?>

        <header>	
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-3">
                        <div id="logo"><a href="<?php echo base_url("portal"); ?>"><img src="<?php echo base_url('public/images/logo-landing.jpg'); ?>"></a></div>
                    </div>
                    <div class="col-xs-12 col-md-6" id="title">
                        <h1>ACCA Online Voting Portal</h1>
                    </div>
                    <div class="col-xs-12 col-md-3 user">
                        <img src="<?php echo base_url('public/images/user.png'); ?>">

                        <div id="member">
                            <h3>Hello <?php echo $username; ?>!</h3>
                        </div>
                    </div> 
                </div>
            </div>
        </header>

        <section class="container content">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <h2>Add candidates</h2>
                </div>



                <div class="col-xs-12 col-md-6">
                    <div class="row">
                        <form role="form" id="add-candidates" method="post" action="<?php echo base_url('candidates/addCandidates'); ?>" autocomplete="off">
                            <fieldset>
                                <div class="form-group">
                                    <select class="form-control" name="title">
                                        <optgroup autofocus>
                                            <option value="Mr">Mr</option>
                                            <option value="Mrs">Mrs</option>
                                            <option value="Miss">Miss</option>
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="first name" name="first_name" type="text" value="" required autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="last name" name="last_name" type="text" value="">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="qualifications" name="qualifications" type="text" value="">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Designation" name="designation" type="text" value="">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Company" name="company" type="text" value="">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="From" name="from" type="text" value="">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="to" name="to" type="text" value="">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="service details" name="service_details" type="text" value="">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Address" name="address" type="text" value="">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Nominated Presenter" name="n_presenter" type="text" value="">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Nominated seconder" name="n_seconder" type="text" value="">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Boigraphy" name="biography"></textarea>
                                </div>
                                <input id="submit" type="submit" value="SAVE" name="save" >

                            </fieldset>
                        </form>
                    </div>  
                </div>

            </div>

        </section>


        <?php require 'global-footer.php'; ?>