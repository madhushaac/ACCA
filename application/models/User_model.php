<?php

class User_model extends CI_model {

    public function downloadUsers() {

        $this->db->select('*');
        $this->db->from('users');

        if ($query = $this->db->get()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getUserVoteStatus($userId) {

        try {

            $this->db->select('vote_status');
            $this->db->from('users');
            $this->db->where('id', $userId);

            if ($query = $this->db->get()) {
                return $query->row_array();
            } else {
                return false;
            }
            
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function insertUsers($userDetails) {
        try {
            
            $this->db->trans_start();
            foreach ($userDetails as $user){
                $result = $this->db->insert('users', $user);
            }
            $this->db->trans_complete();
            
            if ($result) {
                return $result;
            } else {
                return false;
            }
            
            
        } catch (Exception $ex) {
            throw $ex;
        }
    }

}
