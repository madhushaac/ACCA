<?php

class Login_model extends CI_model {

    public function login_user($memId, $pass) {

        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('member_id', $memId);
        $this->db->where('password', $pass);

        if ($query = $this->db->get()) {
            return $query->row_array();
        } else {
            return false;
        }
    }

}
