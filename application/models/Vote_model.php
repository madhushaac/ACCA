<?php

class Vote_model extends CI_model {

    public function addVote($details) {

        try {
            if ($details) {

                if ($details['candidate_id'] && is_array($details['candidate_id'])) {
                    $this->db->trans_start();
                    foreach ($details['candidate_id'] as $candidate) {

                        $data = array(
                            'member_id' => $details['member_id'],
                            'candidate_id' => $candidate
                        );

                        $result = $this->db->insert('votes', $data);
                    }
                    
                    $this->db->set('vote_status', 1);
                    $this->db->where('member_id', $details['member_id']);
                    $result = $this->db->update('users');
                    
                    $this->db->trans_complete();
                }

                if ($result) {
                    return $result;
                } else {
                    return false;
                }
            }
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function getUserVoteStatus($param) {
        try {
            
            
            
        } catch (Exception $exc) {
            throw $ex;
        }
            
    }
    
    public function getAllvotes() {
        try {
            
            $select = array(
                'votes.candidate_id',
                'candidates.first_name',
                'count(member_id) as votes'
            );
            
            $this->db->select($select);
            $this->db->from('votes');   
            $this->db->join('candidates', 'candidates.id = votes.candidate_id');  
            $this->db->group_by('candidate_id');

            if ($query = $this->db->get()) {
                return $query->result_array();
            } else {
                return false;
            }
            
        } catch (Exception $ex) {
            throw $ex;
        }
    }

}
