<?php

class Candidates_model extends CI_model {

    public function addCandidates($candidates = null, $service = null) {

        try {
            if ($candidates) {
                $this->db->trans_start();
                $result = $this->db->insert('candidates', $candidates);
                $insert_id = $this->db->insert_id();

                if ($result && $insert_id && $service) {
                    $newArray = array('user_id' => $insert_id) + $service;
                    $result = $this->db->insert('service_details', $newArray);
                }

                $this->db->trans_complete();
            }

            if ($result) {
                return $insert_id;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function getAllCandidates() {
        try {

            $this->db->select('*');
            $this->db->from('candidates');
            $this->db->join('service_details', 'candidates.id = service_details.user_id');
            $this->db->where('candidates.status = ' . 1);

            if ($query = $this->db->get()) {
                return $query->result_array();
            } else {
                return false;
            }
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function getCandidatesById($user_id) {
        try {

            if ($user_id) {
                $this->db->select('*');
                $this->db->from('candidates');
                $this->db->join('service_details', 'candidates.id = service_details.user_id');
                $this->db->where('candidates.id = ' . $user_id);

                if ($query = $this->db->get()) {
                    return $query->result_array();
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function deleteCandidate($userId) {
        try {

            if ($userId) {
                $this->db->set('status', 0);
                $this->db->where('id', $userId);
                $result = $this->db->update('candidates');

                if ($result) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function updateCandidates($candidates = null, $service = null, $candidate_id) {

        try {
            $result = null;
            if ($candidates) {
                $this->db->trans_start();

                $this->db->where('id', $candidate_id);
                $result = $this->db->update('candidates', $candidates);

                if ($result && $service) {
                    $newArray = array('user_id' => $candidate_id) + $service;
                    $this->db->where('user_id', $candidate_id);
                    $result = $this->db->update('service_details', $newArray);
                }

                $this->db->trans_complete();
            }

            if ($result) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            throw $ex;
        }
    }

}
