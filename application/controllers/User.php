                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->helper('url');
        $this->load->model('login_model');
        $this->load->model('user_model');
        $this->load->library('session');

        $this->user_id = $this->session->userdata('id');

        if (!$this->user_id) {
            redirect('login');
        }
    }

    public function downloadUsers() {
        try {

            if (!$this->user_id) {
                redirect('login');
            } else {
                if (SUPER_USER_ID == $this->user_id) {

                    $result = $this->user_model->downloadUsers();

                    $fp = fopen(base_url() . 'public/user-report.csv', 'w');

                    foreach ($result as $user) {
                        fputcsv($fp, $user);
                    }

                    fclose($fp);
                } else {
                    redirect('portal');
                }
            }
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function addUsers() {
die('addUsers');
        $userArray = array();

        $handle = fopen(base_url() . 'public/users.csv', 'r');
        while ($row = fgetcsv($handle)) {

            $userArray[] = explode(';', $row[0]);
        }

        $userDetails = [];
        foreach ($userArray as $key => $user) {
            $userPassrod = $this->randomPassword();
            $userDetails[$key]['member_id'] = $user[0];
            $userDetails[$key]['first_name'] = $user[1];
            $userDetails[$key]['password'] = md5($userPassrod);
        }

        
        print_r($userDetails); 

//        $result = $this->user_model->insertUsers($userDetails);
    }

    public function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

}
