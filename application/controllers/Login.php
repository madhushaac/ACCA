<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->helper('url');
        $this->load->model('login_model');
        $this->load->library('session');
    }

    public function index() {
        $this->load->view('login');
    }

    public function login_user() {
        try {
            $user_login = array(
                'member_id' => $this->input->post('member_id'),
                'password' => md5($this->input->post('password'))
            );

            $result = $this->login_model->login_user($user_login['member_id'], $user_login['password']);

            if ($result) {

                $this->session->set_userdata('id', $result['member_id']);
                $this->session->set_userdata('first_name', $result['first_name']);
                $this->session->set_userdata('last_name', $result['last_name']);

                redirect('portal');
            } else {
                $data = array();
                $data['log_message'] = 'Incorrect user id or password';
                $this->load->view('login',$data);
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function user_logout() {

        $this->session->sess_destroy();
        redirect('login', 'refresh');
        
    }

}
