<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Vote extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->helper('url');
        $this->load->model('login_model');
        $this->load->model('vote_model');
        $this->load->model('candidates_model');
        $this->load->library('session');

        $this->user_id = $this->session->userdata('id');

        if (!$this->user_id) {
            redirect('login');
        }
    }

    public function index() {
        
    }

    public function addVote() {
        try {
            
            if (!$this->user_id) {
                redirect('login');
            } else {
                $response = array('success' => false, 'message' => '');

                if ($this->input->post('candidate_id')){
                    $details = array(
                        'member_id' => intval($this->user_id),
                        'candidate_id' => ($this->input->post('candidate_id')) ? $this->input->post('candidate_id') : ''
                    );
                    $result = $this->vote_model->addVote($details);
                    if ($result) {
                        $response['success'] = true;
                        $response['message'] = 'Successfully added';
                    } else {
                        $response['message'] = 'Something went wrong';
                    }
                }else{
                    $response['message'] = 'no candidate id found';
                }

                echo json_encode($response);
                exit;
            }
            
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function getAllVotes() {
        try {
            if (!$this->user_id) {
                redirect('login');
            } else {
                if (SUPER_USER_ID == $this->user_id) {
                    $fname = $this->session->userdata('first_name');

                    $data['username'] = $fname;

                    $result = $this->vote_model->getAllVotes();
                    $data['votes'] = $result;
                    $this->load->view('show-votes',$data);
                }else{
                    redirect('portal');
                }
            }
            
        } catch (Exception $ex) {
            throw $ex;
        }
    }

}
