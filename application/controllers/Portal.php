<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portal extends CI_Controller {
    
    public function __construct(){
 
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('login_model');
        $this->load->model('user_model');
        $this->load->model('candidates_model');
        $this->load->library('session');
        
        $user_id = $this->session->userdata('id');
        
        if(!$user_id){
            redirect('login');
        }
 
    }

    public function index()
    {
        $fname = $this->session->userdata('first_name');
        $result = $this->candidates_model->getAllCandidates();
        $voteStatus = $this->user_model->getUserVoteStatus($this->session->userdata('id'));
        
        $data['username'] = $fname;
        $data['user_id'] = $this->session->userdata('id');
        $data['candidates'] = $result;
        $data['voteStatus'] = $voteStatus;
        $this->load->view('portal',$data);
    }
        
}
