<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Candidates extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->helper('url');
        $this->load->model('candidates_model');
        $this->load->library('session');

        $this->user_id = $this->session->userdata('id');
        if (!$this->user_id) {
            redirect('login');
        }
    }

    public function index() {
        $this->candidatesPanelShow();
    }

    public function candidatesPanelShow() {
        if (!$this->user_id) {
            redirect('login');
        } else {
            if (SUPER_USER_ID == $this->user_id) {
                $fname = $this->session->userdata('first_name');
                $result = $this->candidates_model->getAllCandidates();

                $data['username'] = $fname;
                $data['candidates'] = $result;
                $this->load->view('add-candidates', $data);
            } else {
                redirect('portal');
            }
        }
    }

    public function manageCandidates() {
        if (!$this->user_id) {
            redirect('login');
        } else {
            if (SUPER_USER_ID == $this->user_id) {
                $fname = $this->session->userdata('first_name');
                $result = $this->candidates_model->getAllCandidates();

                $data['username'] = $fname;
                $data['candidates'] = $result;
                $this->load->view('manage-candidates', $data);
            } else {
                redirect('portal');
            }
        }
    }

    public function showCandidates() {

        if (!$this->user_id) {
            redirect('login');
        } else {
            $candidates = $this->getAllCandidates();
            $data['candidates'] = $candidates;
            $this->load->view('view-candidates', $data);
        }
    }

    public function addCandidates() {
        try {
            $response = array('success' => false);
            if (!$this->user_id) {
                redirect('login');
            } else {
                if (SUPER_USER_ID == $this->user_id) {
                    $candidate_details = array(
                        'title' => ($this->input->post('title')) ? addslashes($this->input->post('title')) : '',
                        'first_name' => ($this->input->post('first_name')) ? addslashes($this->input->post('first_name')) : '',
                        'last_name' => ($this->input->post('last_name')) ? addslashes($this->input->post('last_name')) : '',
                        'qualifications' => ($this->input->post('qualifications')) ? addslashes($this->input->post('qualifications')) : '',
                        'designation' => ($this->input->post('designation')) ? addslashes($this->input->post('designation')) : '',
                        'company' => ($this->input->post('company')) ? addslashes($this->input->post('company')) : '',
                        'address' => ($this->input->post('address')) ? addslashes($this->input->post('address')) : '',
                        'nominated_presenter' => ($this->input->post('n_presenter')) ? addslashes($this->input->post('n_presenter')) : '',
                        'nominated_seconder' => ($this->input->post('n_seconder')) ? addslashes($this->input->post('n_seconder')) : '',
                        'biography' => ($this->input->post('biography')) ? addslashes($this->input->post('biography')) : ''
                    );

                    $candidate_service_details = array(
                        'from_date' => ($this->input->post('from')) ? addslashes($this->input->post('from')) : '',
                        'to_date' => ($this->input->post('to')) ? addslashes($this->input->post('to')) : '',
                        'service_details' => ($this->input->post('service_details')) ? addslashes($this->input->post('service_details')) : ''
                    );

                    if (!empty($candidate_details['first_name'])) {
                        $result = $this->candidates_model->addCandidates($candidate_details, $candidate_service_details);

                        if ($result) {
                            $response['success'] = true;
                            $response['message'] = 'Candidate details successfully added';
                        } else {
                            $response['message'] = 'Something went wrong';
                        }
                    } else {
                        $response['message'] = 'please fill all details';
                    }
                } else {
                    redirect('portal');
                }
            }
            echo json_encode($response);
            exit;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function getAllCandidates() {
        try {
            if (!$this->user_id) {
                redirect('login');
            } else {
                $result = $this->candidates_model->getAllCandidates();
                if ($result) {
                    return $result;
                } else {
                    return false;
                }
            }
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function getCandidatesById() {
        try {
            if (!$this->user_id) {
                redirect('login');
            } else {
                $response = array('success' => false, 'data' => '', 'message' => '');

                $user_id = ($this->input->post('user_id')) ? intval($this->input->post('user_id')) : '';
                $result = $this->candidates_model->getCandidatesById($user_id);

                if ($result) {
                    $response['success'] = true;
                    $response['data'] = $result;
                } else {
                    $response['message'] = 'Something went wrong';
                }

                echo json_encode($response);
                exit;
            }
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function deleteCandidate() {
        try {
            if (!$this->user_id) {
                redirect('login');
            } else {
                $response = array('success' => false, 'message' => '');
                $userId = ($this->input->post('userId')) ? intval($this->input->post('userId')) : '';
                $result = $this->candidates_model->deleteCandidate($userId);
                if ($result) {
                    $response['success'] = true;
                    $response['message'] = 'Successfully deleted';
                } else {
                    $response['message'] = 'Something went wrong';
                }

                echo json_encode($response);
                exit;
            }
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function updateCandidates() {
        try {
            if (!$this->user_id) {
                redirect('login');
            } else {
                if (SUPER_USER_ID == $this->user_id) {
                    $candidate_details = array(
                        'title' => ($this->input->post('title')) ? addslashes($this->input->post('title')) : '',
                        'first_name' => ($this->input->post('first_name')) ? addslashes($this->input->post('first_name')) : '',
                        'last_name' => ($this->input->post('last_name')) ? addslashes($this->input->post('last_name')) : '',
                        'qualifications' => ($this->input->post('qualifications')) ? addslashes($this->input->post('qualifications')) : '',
                        'designation' => ($this->input->post('designation')) ? addslashes($this->input->post('designation')) : '',
                        'company' => ($this->input->post('company')) ? addslashes($this->input->post('company')) : '',
                        'address' => ($this->input->post('address')) ? addslashes($this->input->post('address')) : '',
                        'nominated_presenter' => ($this->input->post('n_presenter')) ? addslashes($this->input->post('n_presenter')) : '',
                        'nominated_seconder' => ($this->input->post('n_seconder')) ? addslashes($this->input->post('n_seconder')) : '',
                        'biography' => ($this->input->post('biography')) ? addslashes($this->input->post('biography')) : ''
                    );

                    $candidate_service_details = array(
                        'from_date' => ($this->input->post('from')) ? intval($this->input->post('from')) : '',
                        'to_date' => ($this->input->post('to')) ? intval($this->input->post('to')) : '',
                        'service_details' => ($this->input->post('service_details')) ? addslashes($this->input->post('service_details')) : ''
                    );

                    $candidate_id = ($this->input->post('candidate_id')) ? intval($this->input->post('candidate_id')) : '';

                    if (!empty($candidate_details['first_name'])) {
                        $result = $this->candidates_model->updateCandidates($candidate_details, $candidate_service_details, $candidate_id);

                        if ($result) {
                            $response['success'] = true;
                            $response['message'] = 'Candidate details successfully updated';
                        } else {
                            $response['message'] = 'Something went wrong';
                        }
                    } else {
                        $response['message'] = 'please fill all mandatory fields';
                    }

                    echo json_encode($response);
                    exit;
                } else {
                    redirect('portal');
                }
            }
        } catch (Exception $ex) {
            throw $ex;
        }
    }

}
