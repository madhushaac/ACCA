var candidateIdArray = [];

$(document).ready(function () {

    _ACCA.addVotes();
    _ACCA.votePanelShow();
    _ACCA.voteSubmit();

});

_ACCA.addVotes = function () {

    try {
        $('.vote-button').click(function () {

            if (candidateIdArray.length == 4 && !($(this).hasClass('undo'))) {
                alert('You can only vote for 4 candidates');
            } else {
                var candidate = $(this).attr('data-candidate-id');

                if ($(this).hasClass('undo')) {
                    $(this).removeClass('undo');
                    var index = candidateIdArray.indexOf(parseInt(candidate));
                    if (index > -1) {
                        candidateIdArray.splice(index, 1);
                    }

                } else {
                    $(this).addClass('undo');
                    candidateIdArray.push(parseInt(candidate));
                }

            }

            if (candidateIdArray.length > 3) {
                $('#confirm-vote').removeClass('disabled');
            } else {
                $('#confirm-vote').addClass('disabled');
            }
        });

    } catch (e) {
        console.log(e);
    }

};

_ACCA.votePanelShow = function () {
    try {

        $('#confirm-vote').click(function () {

            var candidate_name = '';
            var nameBlock = '';
            candidateIdArray.forEach(function (id) {
                var candidate_name = $('.candidate-holder[data-candidate-id="' + id + '"] .candidate-name').text();
                nameBlock += '<p>' + candidate_name + '</p>';
            });
            $('#vote-confirmation-popup .voted-candidates').html(nameBlock);
            $('#vote-confirmation-popup').modal('show');

            $('#vote-cancle').click(function () {
                $('#vote-confirmation-popup').modal('hide');
            });

        });

    } catch (e) {
        console.log(e);
    }

};

_ACCA.voteSubmit = function () {

    try {

        $('#vote-confirm').click(function () {
            $('#vote-confirmation-popup').modal('hide');

            var data = {
                candidate_id: candidateIdArray
            };

            $.ajax({
                type: 'POST',
                url: 'vote/add-vote',
                data: data,
                dataType: 'json'
            }).done(function (data) {

                if(data.success){
                    $('#vote-success-popup').modal('show');
                    $('#vote-return').click(function(){
                        $('#vote-success-popup').modal('hide');
                        location.reload(); 
                    });
                }

            }).fail(function () {

                alert('something went wrong');

            });

        });

    } catch (e) {
        console.log(e);
    }


};