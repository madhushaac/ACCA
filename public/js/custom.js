(function( $ ){

	/* ----------------------------------------------------------- */
	/* EVENT TIME COUNTER
	/* ----------------------------------------------------------- */
	var mytime = "2018-07-06 10:19:00";
	
	$('#event-counter').countdown(mytime, function(event) {
     	$(this).html(event.strftime(''
	    + '<span class="event-counter-block"><span>%D</span> Days</span> '
	    + '<span class="event-counter-block"><span>%H</span> Hours</span> '
	    + '<span class="event-counter-block"><span>%M</span> Mins</span> '
	    + '<span class="event-counter-block"><span>%S</span> Secs</span>'));			
   	});

})( jQuery );



  
	