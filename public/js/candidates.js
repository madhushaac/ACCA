var _ACCA = new Object();

$(document).ready(function () {

    _ACCA.addCandidates();
    _ACCA.showCandidateDetails();
    _ACCA.deleteCandidates();
    _ACCA.editPanelShow();

});

_ACCA.addCandidates = function () {

    try {

        var form = $('#add-candidates');

        form.on('submit', function (e) {
            e.preventDefault();
            var formData = form.serialize();
            var formUrl = form.attr('action');

            $.ajax({
                type: 'POST',
                url: formUrl,
                data: formData,
                dataType: 'json'
            }).done(function (data) {

                if (data) {
                    if (data.success) {
                        alert(data.message);
                        $("#add-candidates")[0].reset();
                    }
                } else {
                    alert('something went wrong');
                }

            }).fail(function () {

                alert('something went wrong');

            });
        });

    } catch (e) {
        console.log(e);
    }

};

_ACCA.showCandidateDetails = function () {
    try {

        $('.view-profile').click(function () {
            var user_id = $(this).attr('data-candidate-id');
            var data = {
                user_id: user_id
            };

            $.ajax({
                type: 'POST',
                url: 'candidates/get-candidate-details',
                data: data,
                dataType: 'json'
            }).done(function (data) {

                if (data.success) {
                    _ACCA.drawPopup(data.data);
                } else {
                    alert(data.message);
                }

            }).fail(function () {

                alert('something went wrong');

            });

        });

    } catch (e) {
        console.log(e);
    }

};

_ACCA.drawPopup = function (data) {
    try {

        if (data) {
            var template, fromDate, toDate, serviceDetails, designation, company, address, biography, nominated_presenter, nominated_seconder = '';
            data.forEach(function (data) {

                fromDate = (parseInt(data.from_date) !== 0) ? data.from_date : '-';
                toDate = (parseInt(data.to_date) !== 0) ? data.to_date : '-';
                serviceDetails = (data.service_details) ? data.service_details : '-';
                designation = (data.designation) ? data.designation : '-';
                company = (data.company) ? data.company : '-';
                address = (data.address) ? data.address : '-';
                biography = (data.biography) ? data.biography : '-';
                nominated_presenter = (data.nominated_presenter) ? data.nominated_presenter : '-';
                nominated_seconder = (data.nominated_seconder) ? data.nominated_seconder : '-';

                template = '<div class="modal-dialog modal-lg modal-dialog-centered" role="document">\n\
                            <div class="modal-content">\n\
                                <div class="modal-header">\n\
                                    <img src="public/images/logo-landing.jpg" alt="">\n\
                                    <h2>Candidates’ Electon Statements</h2>\n\
                                    <h3>Election to the Sri Lanka Board</h3>\n\
                                    <p>Please read carefully before casting your vote.</p>\n\
                                </div>\n\
                                <div class="modal-body">\n\
                                    <section class="container-fluid ">\n\
                                        <div class="row">\n\
                                            <div class="col-xs-2 profile-img">\n\
                                                <img src="public/images/user.png">\n\
                                            </div>\n\
                                            <div class="col-xs-8 profile-details">\n\
                                                <h2>' + data.title + '. ' + data.first_name + ' ' + data.last_name + ', ' + data.qualifications + '</h2>\n\
                                                <p><span>Job Title:</span> ' + designation + '</p>\n\
                                                <p><span>Name of Company:</span> ' + company + '</p>\n\
                                                <p><span>Address:</span> ' + address + '</p>\n\
                                                <h2 class="sec-title">Details of services to the Institute </h2>\n\
                                                <table style="width:100%">\n\
                                                    <tr>\n\
                                                      <th>From</th>\n\
                                                      <th>To</th> \n\
                                                      <th>Details of Service</th>\n\
                                                    </tr>\n\
                                                    <tr>\n\
                                                      <td>' + fromDate + '</td>\n\
                                                      <td>' + toDate + '</td> \n\
                                                      <td>' + serviceDetails + '</td>\n\
                                                    </tr>\n\
                                                </table>\n\
                                                <h2 class="sec-title">Biographical Details</h2>\n\
                                                <p>' + biography + '</p>\n\
                                                <h2 class="sec-title">Nominated by</h2>\n\
                                                <p><span>Proposer:</span> ' + nominated_presenter + '</p>\n\
                                                <p><span>Seconder:</span> ' + nominated_seconder + '</p>\n\
                                            </div>\n\
                                        </div>\n\
                                    </section>\n\
                                </div>\n\
                            </div>\n\
                        </div>';
            });

            $('#profile-modal').html(template);

            $('#profile-modal').modal('show');
        } else {
            console.log('no data found');
        }



    } catch (e) {
        console.log(e);
    }

};

_ACCA.deleteCandidates = function () {

    $('.user-delete').click(function () {
        var userId = $(this).attr('data-id');
        var data = {
            userId: userId
        };

        $.ajax({
            type: 'POST',
            url: 'delete-candidate',
            data: data,
            dataType: 'json'
        }).done(function (data) {
            if (data.success) {

                $('.candidate-records tr[data-id = "' + userId + '"]').fadeOut();

            } else {
                alert(data.message);
            }

        }).fail(function () {

            alert('something went wrong');

        });

    });


};

_ACCA.editPanelShow = function () {

    try {

        $('.user-edit').click(function () {
            var candidate_id = $(this).attr('data-id');
            var data = {
                user_id: candidate_id
            };

            $.ajax({
                type: 'POST',
                url: 'get-candidate-details',
                data: data,
                dataType: 'json'
            }).done(function (data) {
                if (data.success) {
                    _ACCA.showPopup(data.data, candidate_id);
                } else {
                    alert(data.message);
                }

            }).fail(function () {

                alert('something went wrong');

            });

        });

    } catch (e) {
        console.log(e);
    }


};

_ACCA.showPopup = function (data, candidate_id) {

    try {

        var title, first_name, last_name, qualifications, fromDate, toDate, service_details, designation, company, address, biography, n_presenter, n_seconder = '';
        data.forEach(function (data) {
            title = data.title;
            first_name = data.first_name;
            last_name = data.last_name;
            qualifications = data.qualifications;
            designation = data.designation;
            company = data.company;
            fromDate = (parseInt(data.from_date)) ? data.from_date : '';
            toDate = (parseInt(data.to_date)) ? data.to_date : '';
            service_details = data.service_details;
            address = data.address;
            n_presenter = data.nominated_presenter;
            n_seconder = data.nominated_seconder;
            biography = data.biography;
        });

        $('#candidate-edit-popup #title').val(title);
        $('#candidate-edit-popup #first_name').val(first_name);
        $('#candidate-edit-popup #last_name').val(last_name);
        $('#candidate-edit-popup #qualifications').val(qualifications);
        $('#candidate-edit-popup #designation').val(designation);
        $('#candidate-edit-popup #company').val(company);
        $('#candidate-edit-popup #fromDate').val(fromDate);
        $('#candidate-edit-popup #toDate').val(toDate);
        $('#candidate-edit-popup #service_details').val(service_details);
        $('#candidate-edit-popup #address').val(address);
        $('#candidate-edit-popup #n_presenter').val(n_presenter);
        $('#candidate-edit-popup #n_seconder').val(n_seconder);
        $('#candidate-edit-popup #biography').val(biography);

        $('#candidate-edit-popup').modal('show');

        var form = $('#update-candidates');

        form.on('submit', function (e) {
            e.preventDefault();
            var formData = form.serialize();
            formData = formData + '&candidate_id=' + candidate_id;
            var formUrl = form.attr('action');
            _ACCA.updateCandidates(formData, formUrl);
        });
        

    } catch (e) {
        console.log(e);
    }


};

_ACCA.updateCandidates = function (formData, formUrl) {
    try {

        $.ajax({
            type: 'POST',
            url: formUrl,
            data: formData,
            dataType: 'json'
        }).done(function (data) {

            if (data) {
                if (data.success) {
                    alert(data.message);
                    $("#update-candidates")[0].reset();
                    $('#candidate-edit-popup').modal('hide');
                }
            } else {
                alert('something went wrong');
            }

        }).fail(function () {

            alert('something went wrong');

        });

    } catch (e) {
        console.log(e);
    }

};

_ACCA.showCandidateDetails = function () {
    try {

        $('.view-profile').click(function () {
            var user_id = $(this).attr('data-candidate-id');
            var data = {
                user_id: user_id
            };

            $.ajax({
                type: 'POST',
                url: 'candidates/get-candidate-details',
                data: data,
                dataType: 'json'
            }).done(function (data) {

                if (data.success) {
                    _ACCA.drawPopup(data.data);
                } else {
                    alert(data.message);
                }

            }).fail(function () {

                alert('something went wrong');

            });

        });

    } catch (e) {
        console.log(e);
    }

};